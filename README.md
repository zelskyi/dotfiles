# Zelskyi dotfiles
* * *
![FireFox](https://gitlab.com/zelskyi/dotfiles/raw/a023def40425486307da6780bd99b3f30a0cf94a/1.png)

![ScreenFetch](https://gitlab.com/zelskyi/dotfiles/raw/a023def40425486307da6780bd99b3f30a0cf94a/2.png)

![WorkSpace](https://gitlab.com/zelskyi/dotfiles/raw/a023def40425486307da6780bd99b3f30a0cf94a/3.png)

![vk-cli](https://gitlab.com/zelskyi/dotfiles/raw/a023def40425486307da6780bd99b3f30a0cf94a/4.png)

![rofi](https://gitlab.com/zelskyi/dotfiles/raw/a023def40425486307da6780bd99b3f30a0cf94a/5.png)
# Requirements
* VIM Vundle
* Droid Sans font
* Conky
* Git
* Compton
* dbus
* i3blocks
* i3status
* Awesome Font
* System San Francisci Font
* twmn
* rofi (like dmenu)
* Arc Theme for GTK2(3) + Firefox + Arc Icons
# Instalation 
* * *
Just add `git clone git@gitlab.com:zelskyi/dotfiles.git ~/`. Then use `vim` for `:PluginInstall`. 
And for conky in bash type `conky`.

# Feedback
* * * 
If you have question or suggestions/improvements please write me [welcome](https://gitlab.com/zelskyi/dotfiles/issues)!
