" Vundle
set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'airblade/vim-gitgutter'

" All plugins up
call vundle#end()            " required
filetype plugin indent on    " required

" Colorscheme
colorscheme monokai
let g:airline_powerline_fonts = 1
set t_Co=256
:let g:airline_theme='badwolf' 
:let ttimeoutlen=50
:let g:airline#extensions#hunks#enabled=1
:let g:airline#extensions#branch#enabled=1

map q <Nop>
map <F1> <Nop>

" Leader = ,
let mapleader=","
let NERDTreeQuitOnOpen = 1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
let NERDTreeIgnore = ['\.pyc$', '\.retry$']
nmap <silent> <leader><leader> :NERDTreeToggle<CR>

" Map ctrl-movement keys to window switching
map <C-k> <C-w><Up>
map <C-j> <C-w><Down>
map <C-l> <C-w><Right>
map <C-h> <C-w><Left>


set nocompatible " сумісність з ВІ
set laststatus=2
set nu
set shiftwidth=4 " розмір відступу <<>>
set tabstop=4 " ширина табуляції
set autoindent " авто відступи
set expandtab " табуляція в пробіли
set iminsert=0 " дефолтна мова - англійська
set imsearch=0 " аналогічно для строки введеня команд
set encoding=utf-8 " без коментарів
set fileencoding=utf-8 " зрозуміло
set noswapfile " не використовувари .swp файли
set scrolloff=4 " рядків при прокрутці
set cursorline " горизонтальна полоса в рядку курсору

" ГАРЯЧІ КЛАВІШІ
" Shitf+Tab - закртити  файл
nmap <S-Tab> :q<cr>
vmap <S-Tab> <esc>:q<cr>i
imap <S-TAb> <esc>:q<cr>i
"
" F2 - зберегти  файл
nmap <F2> :w<cr>
vmap <F2> <esc>:w<cr>i
imap <F2> <esc>:w<cr>i
" F5 - перегляд буферів
nmap <F1> <Esc>:buffers<cr>
vmap <F1> <esc>:buffers<cr>
imap <F1> <esc><esc>:buffers<cr>a
" F3 попередній буфер
map <F3> :bp<cr>
vmap <F3> <esc>:bp<cr>i
imap <F3> <esc>:bp<cr>i
" F4 - наступний буфер
map <F4> :bn<cr>
vmap <F4> <esc>:bn<cr>i
imap <F4> <esc>:bn<cr>i
" On syntax
syntax on

" Nertre tagbar

